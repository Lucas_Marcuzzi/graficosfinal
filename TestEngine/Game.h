#pragma once

#include "GameBase.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Circle.h"
#include "Sprite.h"
#include "Tilemap.h"
#include "Input.h"
#include "CollisionManager.h"
#include "Player.h"
#include "NewCamera.h"
#include "Asteroid.h"
#include "Cube.h"
#include "Mesh_NoAssimp.h"
#include "Mesh.h"
#include "Model.h"
#include "ModelImporter.h"
#include "Coso.h"
#include "Transform.h"
#include "MaterialComponent.h"
#include "MeshRenderer.h"
#include "Componente.h"
#include "CameraComponent.h"
#include "TreeModel.h"
#include "BSPPlane.h"
#include "BSPPlaneBulk.h"
#include "DebugMessageManager.h"

enum GameState
{
	LOSE = -1,
	CONTINUE,
	WIN
};

class Game : public GameBase
{
	Input* input;
	Material* material;
	Material* matTexture;
	Tilemap* tilemap;
	NewCamera* camera;
	Coso* cameraCoso;
	Coso* BSPTile1;
	Coso* BSPTile2;
	Coso* BSPTile3;
	Coso* BSPTile4;
	Coso* cubo1;
	Coso* cubo2;
	Coso* cubo3;
	Coso* cubo4;
	Coso* BSPBulk;
	Coso* paredes;
	Renderer* renderer;

	float speed;		// Speed

	const int totalAsteroids = 25;

	GameState gameState;

	void FillAsteroidsData();
	void Restart();

protected:
	bool OnStart() override;
	bool OnStop() override;
	bool OnUpdate() override;
	bool OnDraw() override;

public:
	Game();
	~Game();
};

