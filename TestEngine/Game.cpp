#include "Game.h"
#define CAMERASPEED_1 1.0f
#define CAMERASPEED_2 0.01f

Game::Game()
{
}

Game::~Game()
{
	delete cameraCoso;
	if (GetRenderer()->dmm.finalExam == GRAFICOS4)
	{
		if (renderer->dmm.bulkMode)
		{
			delete BSPBulk;
			delete paredes;
		}
		else
		{
			delete paredes;
			delete BSPTile1;
			delete BSPTile2;
			delete BSPTile3;
			delete BSPTile4;
		}

		delete cubo1;
		delete cubo2;
		delete cubo3;
		delete cubo4;
	}
	if (GetRenderer()->dmm.finalExam == GRAFICOS3)
	{
		delete cubo1;
		delete cubo2;
		delete cubo3;
	}
}

bool Game::OnStart()
{
	renderer = GetRenderer();
	cameraCoso = new Coso(GetRenderer(), "Camera");
	cameraCoso->AddComponent(new CameraComponent(cameraCoso->GetTransform(), GetRenderer()));
	CameraComponent* camcomp = (CameraComponent*)cameraCoso->GetComponent("CameraComponent");
	camcomp->SetPerspective();

	/////////////////////////////SETTINGS////////////////////////////////////////////
	renderer->dmm.finalExam = GRAFICOS4;
	renderer->dmm.bulkMode = false;
	renderer->dmm.neighborSystem = false;
	/////////////////////////SETTINGS END////////////////////////////////////////////

	input = Input::getInstance();
	input->SetWindowContext(GetWindow());

	material = new Material();
	material->LoadShader("Shaders\\SimpleVertexShader.vertexshader"		// Vertex Shader
					   , "Shaders\\SimpleFragmentShader.fragmentshader"	// Fragment Shader
	);

	if (GetRenderer()->dmm.finalExam == GRAFICOS4)
	{
		GetRenderer()->dmm.SetMode(BSP);
		camcomp->SetBSPActive(true);

		GetRenderer()->UnlockBspYAxis(true);
		BSPTile1 = new Coso(GetRenderer(), "BSPTile1");
		BSPTile1->AddComponent(new BSPPlane(BSPTile1->GetTransform(), GetRenderer(), material, "BSPPlane.obj"));
		BSPTile1->GetTransform()->Scale(vec3(20, 20, 20));
		BSPPlane* planesettings1 = (BSPPlane*)BSPTile1->GetComponent("BSPPlane");
		BSPTile2 = new Coso(GetRenderer(), "BSPTile2");
		BSPTile2->AddComponent(new BSPPlane(BSPTile2->GetTransform(), GetRenderer(), material, "BSPPlane.obj"));
		BSPTile2->GetTransform()->Scale(vec3(20, 20, 20));
		BSPTile2->GetTransform()->Translate(45, 0, 0);
		BSPPlane* planesettings2 = (BSPPlane*)BSPTile2->GetComponent("BSPPlane");
		BSPTile3 = new Coso(GetRenderer(), "BSPTile3");
		BSPTile3->AddComponent(new BSPPlane(BSPTile3->GetTransform(), GetRenderer(), material, "BSPPlane.obj"));
		BSPTile3->GetTransform()->Scale(vec3(20, 20, 20));
		BSPTile3->GetTransform()->Translate(-45, 0, 0);
		BSPPlane* planesettings3 = (BSPPlane*)BSPTile3->GetComponent("BSPPlane");
		BSPTile4 = new Coso(GetRenderer(), "BSPTile4");
		BSPTile4->AddComponent(new BSPPlane(BSPTile4->GetTransform(), GetRenderer(), material, "BSPPlane.obj"));
		BSPTile4->GetTransform()->Scale(vec3(60, 20, 20));
		BSPTile4->GetTransform()->Translate(0, 0, 45);
		BSPPlane* planesettings4 = (BSPPlane*)BSPTile4->GetComponent("BSPPlane");


		//NEIGHBOR DEFINITION
		if (renderer->dmm.neighborSystem && !renderer->dmm.bulkMode)
		{
			planesettings2->AddNeighbor(planesettings1->GetBoundingBox());
			planesettings2->AddNeighbor(planesettings4->GetBoundingBox());

			planesettings1->AddNeighbor(planesettings2->GetBoundingBox());
			planesettings1->AddNeighbor(planesettings3->GetBoundingBox());
			planesettings1->AddNeighbor(planesettings4->GetBoundingBox());

			planesettings3->AddNeighbor(planesettings1->GetBoundingBox());
			planesettings3->AddNeighbor(planesettings4->GetBoundingBox());

			planesettings4->AddNeighbor(planesettings2->GetBoundingBox());
			planesettings4->AddNeighbor(planesettings3->GetBoundingBox());
			planesettings4->AddNeighbor(planesettings1->GetBoundingBox());
		}
		
		if (renderer->dmm.bulkMode)
		{
			BSPBulk = new Coso(GetRenderer(), "BSPBulk");
			BSPBulk->AddComponent(new BSPPlaneBulk(BSPBulk->GetTransform(), GetRenderer(), material, "BSPBulk.obj"));
			BSPBulk->GetTransform()->Scale(5, 5, 5);

			paredes = new Coso(GetRenderer(), "paredes");
			paredes->AddComponent(new MeshRenderer(paredes->GetTransform(), GetRenderer(), material, "walls.obj"));
			paredes->GetTransform()->Scale(5, 5, 5);

			cubo1 = new Coso(GetRenderer(), "cubo1");
			cubo1->AddComponent(new MeshRenderer(cubo1->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
			cubo1->GetTransform()->Scale(5, 5, 5);
			cubo1->GetTransform()->Teleport(0, 0, -25);
			cubo2 = new Coso(GetRenderer(), "cubo2");
			cubo2->AddComponent(new MeshRenderer(cubo2->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
			cubo2->GetTransform()->Teleport(-50, -0, -25);
			cubo2->GetTransform()->Scale(5, 5, 5);
			cubo3 = new Coso(GetRenderer(), "cubo3");
			cubo3->AddComponent(new MeshRenderer(cubo3->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
			cubo3->GetTransform()->Teleport(50, 0, -25);
			cubo3->GetTransform()->Scale(5, 5, 5);
			cubo4 = new Coso(GetRenderer(), "cubo4");
			cubo4->AddComponent(new MeshRenderer(cubo4->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
			cubo4->GetTransform()->Teleport(0, 0, 25);
			cubo4->GetTransform()->Scale(5, 5, 5);
		}
		else
		{
			paredes = new Coso(GetRenderer(), "paredes");
			paredes->AddComponent(new MeshRenderer(paredes->GetTransform(), GetRenderer(), material, "walls.obj"));
			paredes->GetTransform()->Scale(3, 4, 4);
			paredes->GetTransform()->Translate(vec3(0, 0, 22));
			cubo1 = new Coso(GetRenderer(), "cubo1");
			cubo1->AddComponent(new MeshRenderer(cubo1->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
			cubo1->GetTransform()->Scale(5, 5, 5);
			cubo1->GetTransform()->Teleport(BSPTile1->GetTransform()->GetPosition().x, BSPTile1->GetTransform()->GetPosition().y, BSPTile1->GetTransform()->GetPosition().z);
			cubo2 = new Coso(GetRenderer(), "cubo2");
			cubo2->AddComponent(new MeshRenderer(cubo2->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
			cubo2->GetTransform()->Teleport(BSPTile2->GetTransform()->GetPosition().x, BSPTile2->GetTransform()->GetPosition().y, BSPTile2->GetTransform()->GetPosition().z);
			cubo2->GetTransform()->Scale(5, 5, 5);
			cubo3 = new Coso(GetRenderer(), "cubo3");
			cubo3->AddComponent(new MeshRenderer(cubo3->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
			cubo3->GetTransform()->Teleport(BSPTile3->GetTransform()->GetPosition().x, BSPTile3->GetTransform()->GetPosition().y, BSPTile3->GetTransform()->GetPosition().z);
			cubo3->GetTransform()->Scale(5, 5, 5);
			cubo4 = new Coso(GetRenderer(), "cubo4");
			cubo4->AddComponent(new MeshRenderer(cubo4->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
			cubo4->GetTransform()->Teleport(BSPTile4->GetTransform()->GetPosition().x, BSPTile4->GetTransform()->GetPosition().y, BSPTile4->GetTransform()->GetPosition().z);
			cubo4->GetTransform()->Scale(5, 5, 5);
		}
	}
	if (GetRenderer()->dmm.finalExam == GRAFICOS3)
	{
		GetRenderer()->dmm.SetMode(MESHCOUNT);
		camcomp->SetBSPActive(false);

		cubo1 = new Coso(GetRenderer(), "cubo1");
		cubo1->AddComponent(new MeshRenderer(cubo1->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
		cubo1->GetTransform()->Scale(5, 5, 5);
		cubo2 = new Coso(GetRenderer(), "cubo2");
		cubo2->AddComponent(new MeshRenderer(cubo2->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
		cubo2->GetTransform()->Teleport(-10,0,0);
		cubo2->GetTransform()->Scale(5, 5, 5);
		cubo3 = new Coso(GetRenderer(), "cubo3");
		cubo3->AddComponent(new MeshRenderer(cubo3->GetTransform(), GetRenderer(), material, camcomp, "cube.obj", "default.bmp"));
		cubo3->GetTransform()->Teleport(10,0,0);
		cubo3->GetTransform()->Scale(5, 5, 5);
	}

	return true;
}

void Game::FillAsteroidsData()
{
	
}

bool Game::OnStop()
{

	return true;
}

bool Game::OnUpdate()
{
	renderer->dmm.Reset();
	CameraComponent* camcomp = (CameraComponent*)(cameraCoso->GetComponent("CameraComponent"));

		if (input->isInput(GLFW_KEY_SPACE))
			camcomp->Strafe(0, CAMERASPEED_1);

		if (input->isInput(GLFW_KEY_LEFT_CONTROL))
			camcomp->Strafe(0, -CAMERASPEED_1);

		if (input->isInput(GLFW_KEY_Q))
			camcomp->Strafe(-CAMERASPEED_1, 0);

		if (input->isInput(GLFW_KEY_E))
			camcomp->Strafe(CAMERASPEED_1, 0);

		if (input->isInput(GLFW_KEY_W))
			camcomp->Walk(CAMERASPEED_1);

		if (input->isInput(GLFW_KEY_S))
			camcomp->Walk(-CAMERASPEED_1);

		if (input->isInput(GLFW_KEY_D))
			camcomp->Yaw(-CAMERASPEED_2);
		
		if (input->isInput(GLFW_KEY_A))
			camcomp->Yaw(CAMERASPEED_2);

		if (input->isInput(GLFW_KEY_LEFT))
			camcomp->Roll(-CAMERASPEED_2);

		if (input->isInput(GLFW_KEY_RIGHT))
			camcomp->Roll(CAMERASPEED_2);

		if (input->isInput(GLFW_KEY_UP))
			camcomp->Pitch(CAMERASPEED_2);

		if (input->isInput(GLFW_KEY_DOWN))
			camcomp->Pitch(-CAMERASPEED_2);

		if (input->isInput(GLFW_KEY_1))
			camcomp->StrafeAdvance(CAMERASPEED_1);

		if (input->isInput(GLFW_KEY_2))
			camcomp->StrafeAdvance(-CAMERASPEED_1);

		if (GetRenderer()->dmm.finalExam == GRAFICOS4)
		{
			if (input->isInput(GLFW_KEY_G))
				renderer->debugShowBSP = !renderer->debugShowBSP;

			if (input->isInput(GLFW_KEY_L))
			{
				cubo1->GetTransform()->Translate(vec3(-0.2f, 0, 0));
			}
			if (input->isInput(GLFW_KEY_K))
			{
				cubo1->GetTransform()->Translate(vec3(0, 0, -0.2f));
			}
			if (input->isInput(GLFW_KEY_I))
			{
				cubo1->GetTransform()->Translate(vec3(0, 0, 0.2f));
			}
			if (input->isInput(GLFW_KEY_J))
			{
				cubo1->GetTransform()->Translate(vec3(0.2f, 0, 0));
			}
		}
		
		input->PollEvents();

		cameraCoso->Update();


		if (renderer->dmm.finalExam == GRAFICOS4)
		{
			if (renderer->dmm.bulkMode)
			{
				BSPBulk->Update();
				paredes->Update();
			}
			else
			{
				BSPTile1->Update();
				BSPTile2->Update();
				BSPTile3->Update();
				BSPTile4->Update();
				paredes->Update();
			}

			cubo1->Update();
			cubo2->Update();
			cubo3->Update();
			cubo4->Update();
		}
		if (GetRenderer()->dmm.finalExam == GRAFICOS3)
		{
			cubo1->Update();
			cubo2->Update();
			cubo3->Update();
		}


		GetRenderer()->dmm.Update();

		return true;

}

bool Game::OnDraw()
{
	return true;
}

void Game::Restart()
{

}