#include "CameraComponent.h"


CameraComponent::CameraComponent(Transform* _transform, Renderer * _renderer)
{
	key = "CameraComponent";
	renderer = _renderer;
	transform = _transform;
	//eyePosition = vec3(0.0f, 0.0f, 10.0f);
	eyePosition = renderer->GetCameraPosition();
	upVector = vec3(0.0f, 1.0f, 0.0f);
	forward = vec4(0.0f, 0.0f, 1.0f, 0.0f);
	right = vec4(1.0f, 0.0f, 0.0f, 0.0f);
	up = vec4(0.0f, 1.0f, 0.0f, 0.0f);

	cameraPosition = eyePosition + (vec3)forward;

	//Camera frustum Parameters
	near = 0.1f;
	far = 1000.0f;
	aspectRatioWidth = 16.0f;
	aspectRatioHeight = 9.0f;
	fov = radians(45.0f);
	tang = tan(fov * 0.5f);
	nearHeight = near * tang;
	nearWidht = nearHeight * (aspectRatioWidth/aspectRatioHeight);

	SetFrustum();
}

void CameraComponent::Update()
{
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);
}


CameraComponent::~CameraComponent()
{
}

vec4 CameraComponent::GeneratePlane(vec3 _normal, vec3 _point)
{
	vec4 plane;

	plane.x = _normal.x;
	plane.y = _normal.y;
	plane.z = _normal.z;
	plane.w = -dot(_normal, _point);

	return plane;
}

void CameraComponent::SetFrustum()
{
	vec3 frontCenter = (vec3)cameraPosition + (vec3)forward * near;
	vec3 backCenter = (vec3)cameraPosition + (vec3)forward * far;

	vec3 leftPlaneVec = (frontCenter - (vec3)right * nearWidht) - (vec3)cameraPosition;
	vec3 rightPlaneVec = (frontCenter + (vec3)right * nearWidht) - (vec3)cameraPosition;
	vec3 topPlaneVec = (frontCenter + (vec3)up * nearHeight) - (vec3)cameraPosition;
	vec3 bottomPlaneVec = (frontCenter - (vec3)up * nearHeight) - (vec3)cameraPosition;

	vec3 normalLeft = normalize(cross(leftPlaneVec, (vec3)up));
	vec3 normalRight = -normalize(cross(rightPlaneVec, (vec3)up));
	vec3 normalTop = normalize(cross(topPlaneVec, (vec3)right));
	vec3 normalBottom = -normalize(cross(bottomPlaneVec, (vec3)right));

	frustumPlanes[NEAR] = GeneratePlane(-(vec3)forward, frontCenter);
	frustumPlanes[FAR] = GeneratePlane((vec3)forward, backCenter);
	frustumPlanes[LEFT] = GeneratePlane(normalLeft, (vec3)cameraPosition);
	frustumPlanes[RIGHT] = GeneratePlane(normalRight, (vec3)cameraPosition);
	frustumPlanes[TOP] = GeneratePlane(normalTop, (vec3)cameraPosition);
	frustumPlanes[BOTTOM] = GeneratePlane(normalBottom, (vec3)cameraPosition);
}

void CameraComponent::SetAspectRatio(float _height, float _width)
{
	near = 0.1f;
	far = 1000.0f;
	aspectRatioWidth = _width;
	aspectRatioHeight = _height;
	fov = radians(45.0f);
	tang = tan(fov * 0.5f);
	nearHeight = near * tang;
	nearWidht = nearHeight * (aspectRatioWidth / aspectRatioHeight);
}

bool CameraComponent::FrustumCheck(vector<vec3>* boundingBox)
{
	if (boundingBox == nullptr)
	{
		return true;
	}

	bool isInsideFrustum = true;
	bool allOutsideFrustum = false;
	for (int i = 0; i < (int)FrustumPlanesEnums::CANT; i++)
	{
		allOutsideFrustum = false;
		float distance[8];

		for (int j = 0; j < boundingBox->size(); j++)
		{
			distance[j] = dot(vec3(frustumPlanes[i]), boundingBox->at(j)) + frustumPlanes[i].w;
			if (distance[j] < 0.0f)
				break;
			if (j == boundingBox->size() - 1)
			{
				allOutsideFrustum = true;
			}
		}
		if (allOutsideFrustum)
		{
			isInsideFrustum = false;
			break;
		}
	}
	return isInsideFrustum;
}

void CameraComponent::SetBSPActive(bool active)
{
	activeBSP = active;
}

void CameraComponent::Strafe(float x, float y)
{
	/*renderer->MoveCamera(glm::vec3(x, y, 0));		
	eyePosition = renderer->GetCameraPosition();
	cameraPosition = renderer->GetRealCameraPosition();*/

	cameraPosition.x += x;
	cameraPosition.y += y;
	eyePosition.x += x;
	eyePosition.y += y;
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);

	SetFrustum();

}

void CameraComponent::SetOrtho()
{
	renderer->SetOrtho();
}

void CameraComponent::SetPerspective()
{
	renderer->SetPerspective(fov,aspectRatioWidth,aspectRatioHeight,near,far);
}

void CameraComponent::Walk(int ammount)
{
	cameraPosition += (vec3)forward*ammount;
	eyePosition += (vec3)forward*ammount;
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);

	SetFrustum();
}

void CameraComponent::StrafeAdvance(int ammount)
{
	cameraPosition.z += ammount;
	eyePosition.z += ammount;
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);

	SetFrustum();
}

void CameraComponent::Pitch(float ammount)
{
	forward = glm::rotate(mat4(1.0f), ammount, vec3(right.x, right.y, right.z)) * forward;
	up = glm::rotate(mat4(1.0f), ammount, vec3(right.x, right.y, right.z)) * up;
	upVector = (vec3)up;
	cameraPosition = eyePosition + (vec3)forward;
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);

	SetFrustum();
}

void CameraComponent::Yaw(float ammount)
{
	forward = glm::rotate(mat4(1.0f), ammount, vec3(upVector.x, upVector.y, upVector.z)) * forward;
	right = glm::rotate(mat4(1.0f), ammount, vec3(upVector.x, upVector.y, upVector.z)) * right;
	upVector = (vec3)up;
	cameraPosition = eyePosition + (vec3)forward;
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);

	SetFrustum();
}

void CameraComponent::Roll(float ammount)
{
	mat4 rot = rotate(mat4(1.0f), ammount, vec3(forward.x, forward.y, forward.z));
	right = rot * right;
	up = rot * up;
	upVector = (vec3)up;
	cameraPosition = eyePosition + (vec3)forward;
	renderer->UpdateViewMatrix(eyePosition, cameraPosition, up);
	renderer->SyncCameras(cameraPosition, eyePosition, up, forward);

	SetFrustum();
}

vector<vector<vec3>*> CameraComponent::GetCurrentBSP()
{
	vector<vector<vec3>*>* BSPList = renderer->GetBSPList();
	bool unlockedY = renderer->GetUnlockedYAxis();

	for (size_t i = 0; i < BSPList->size(); i++)
	{
		//Desprecio mi creaci�n con cada fibra de mi ser, pero no se me occure una manera mas c�mo de hacer esto ahora mismo y no quiero tener un if con 2000000 clausulas.

		if (cameraPosition.x >= BSPList->at(i)->at(TOPLEFTFRONT).x && (cameraPosition.y <= BSPList->at(i)->at(TOPLEFTFRONT).y || unlockedY) && cameraPosition.z >= BSPList->at(i)->at(TOPLEFTFRONT).z) //TOPLEFTFRONT
			if (cameraPosition.x <= BSPList->at(i)->at(TOPRIGHTFRONT).x && (cameraPosition.y <= BSPList->at(i)->at(TOPRIGHTFRONT).y || unlockedY) && cameraPosition.z >= BSPList->at(i)->at(TOPRIGHTFRONT).z) //TOPRIGHTFRONT
				if (cameraPosition.x >= BSPList->at(i)->at(BOTTOMLEFTFRONT).x && (cameraPosition.y >= BSPList->at(i)->at(BOTTOMLEFTFRONT).y || unlockedY) && cameraPosition.z >= BSPList->at(i)->at(BOTTOMLEFTFRONT).z) //BOTTOMLEFTFRONT
					if (cameraPosition.x <= BSPList->at(i)->at(BOTTOMRIGHTFRONT).x && (cameraPosition.y >= BSPList->at(i)->at(BOTTOMRIGHTFRONT).y || unlockedY) && cameraPosition.z >= BSPList->at(i)->at(BOTTOMRIGHTFRONT).z) //BOTTOMRIGHTFRONT
						if (cameraPosition.x >= BSPList->at(i)->at(TOPLEFTBACK).x && (cameraPosition.y <= BSPList->at(i)->at(TOPLEFTBACK).y || unlockedY) && cameraPosition.z <= BSPList->at(i)->at(TOPLEFTBACK).z) //TOPLEFTBACK
							if (cameraPosition.x <= BSPList->at(i)->at(TOPRIGHTBACK).x && (cameraPosition.y <= BSPList->at(i)->at(TOPRIGHTBACK).y || unlockedY) && cameraPosition.z <= BSPList->at(i)->at(TOPRIGHTBACK).z) //TOPRIGHTBACK
								if (cameraPosition.x >= BSPList->at(i)->at(BOTTOMLEFTBACK).x && (cameraPosition.y >= BSPList->at(i)->at(BOTTOMLEFTBACK).y || unlockedY) && cameraPosition.z <= BSPList->at(i)->at(BOTTOMLEFTBACK).z) //BOTTOMLEFTBACK
									if (cameraPosition.x <= BSPList->at(i)->at(BOTTOMRIGHTBACK).x && (cameraPosition.y >= BSPList->at(i)->at(BOTTOMRIGHTBACK).y || unlockedY) && cameraPosition.z <= BSPList->at(i)->at(BOTTOMRIGHTBACK).z) //BOTTOMRIGHTBACK
									{
										renderer->dmm.SetInBSP(true);
										return renderer->GetBoundingBoxAndNeighbors(i);
									}
	}

	renderer->dmm.SetInBSP(false);
	vector<vector<vec3>*> vec;
	return vec;
}

bool CameraComponent::BSPCheck(vector<vec3>* boundingBox)
{
	vector<vector<vec3>*> Bsp = GetCurrentBSP();
	bool unlockedY = renderer->GetUnlockedYAxis();

	//Desprecio mi creaci�n con cada fibra de mi ser, pero no se me occure una manera mas c�mo de hacer esto ahora mismo y no quiero tener un if con 2000000 clausulas.
	if (Bsp.size() > 0)
	{
		for (size_t e = 0; e < Bsp.size(); e++)
		{
			for (size_t i = 0; i < boundingBox->size(); i++)
			{
				if (boundingBox->at(i).x >= Bsp[e]->at(TOPLEFTFRONT).x && (boundingBox->at(i).y <= Bsp[e]->at(TOPLEFTFRONT).y || unlockedY) && boundingBox->at(i).z >= Bsp[e]->at(TOPLEFTFRONT).z) //TOPLEFTFRONT
					if (boundingBox->at(i).x <= Bsp[e]->at(TOPRIGHTFRONT).x && (boundingBox->at(i).y <= Bsp[e]->at(TOPRIGHTFRONT).y || unlockedY) && boundingBox->at(i).z >= Bsp[e]->at(TOPRIGHTFRONT).z) //TOPRIGHTFRONT
						if (boundingBox->at(i).x >= Bsp[e]->at(BOTTOMLEFTFRONT).x && (boundingBox->at(i).y >= Bsp[e]->at(BOTTOMLEFTFRONT).y || unlockedY) && boundingBox->at(i).z >= Bsp[e]->at(BOTTOMLEFTFRONT).z) //BOTTOMLEFTFRONT
							if (boundingBox->at(i).x <= Bsp[e]->at(BOTTOMRIGHTFRONT).x && (boundingBox->at(i).y >= Bsp[e]->at(BOTTOMRIGHTFRONT).y || unlockedY) && boundingBox->at(i).z >= Bsp[e]->at(BOTTOMRIGHTFRONT).z) //BOTTOMRIGHTFRONT
								if (boundingBox->at(i).x >= Bsp[e]->at(TOPLEFTBACK).x && (boundingBox->at(i).y <= Bsp[e]->at(TOPLEFTBACK).y || unlockedY) && boundingBox->at(i).z <= Bsp[e]->at(TOPLEFTBACK).z) //TOPLEFTBACK
									if (boundingBox->at(i).x <= Bsp[e]->at(TOPRIGHTBACK).x && (boundingBox->at(i).y <= Bsp[e]->at(TOPRIGHTBACK).y || unlockedY) && boundingBox->at(i).z <= Bsp[e]->at(TOPRIGHTBACK).z) //TOPRIGHTBACK
										if (boundingBox->at(i).x >= Bsp[e]->at(BOTTOMLEFTBACK).x && (boundingBox->at(i).y >= Bsp[e]->at(BOTTOMLEFTBACK).y || unlockedY) && boundingBox->at(i).z <= Bsp[e]->at(BOTTOMLEFTBACK).z) //BOTTOMLEFTBACK
											if (boundingBox->at(i).x <= Bsp[e]->at(BOTTOMRIGHTBACK).x && (boundingBox->at(i).y >= Bsp[e]->at(BOTTOMRIGHTBACK).y || unlockedY) && boundingBox->at(i).z <= Bsp[e]->at(BOTTOMRIGHTBACK).z) //BOTTOMRIGHTBACK
												return true;
			}
		}
		return false;
	}
	else
	{
		if (activeBSP)
			return false;
		else
			return true;
	}
}

