#pragma once
#include "Exports.h"
#include "Componente.h"
#include "MaterialComponent.h"
#include "Shape.h"
#include "Definitions.h"
#include "ModelImporter.h"
#include "Model.h"
#include "TextureImporter.h"
#include "Transform.h"
#include "CameraComponent.h"

class ENGINEDLL_API BSPPlane :
	public Componente
{

	int planeid;
protected:
	char* path;
	vector<MeshEntry> vecMeshEntry;
	vector<Header> vecHeaders;
	vector<vec3> boundingBox;
	vector<vec3> ogBoundingBox;
	vector<unsigned int> bufferTextureID;
	Renderer* renderer;
	Transform* transform;
	Material* material;
	CameraComponent* frustumCamera = nullptr;
public:
	BSPPlane::BSPPlane(Transform* _transform, Renderer* _renderer, Material* _material, string filepath = "cube.obj", string texturePath = "default.bmp");
	~BSPPlane();
	vector<vec3>* GetBoundingBox();
	void Update() override;
	void Draw();
	void AddNeighbor(vector<vec3>* _new);
};
