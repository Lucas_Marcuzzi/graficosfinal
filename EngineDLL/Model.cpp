#include "Model.h"
#include "BoundingBoxEnums.h"

Model * Model::instance = NULL;

Model::Model()
{
}

Model::~Model()
{
}

Model* Model::GetInstance() //Devuelve la instancia actual del modelo.
{
	if (instance == NULL) //si no existe...
		instance = new Model(); //lo crea
	return instance;
}

void Model::LoadMesh(const string& modelPath, string& texpath, vector<MeshEntry>& meshEntryVec, vector<Header>& textures, vector<vec3> &boundingBox, Renderer* rnd)
{
	Assimp::Importer importer; //Creo el importer de Assimp
	const aiScene* scene = importer.ReadFile(
		modelPath.c_str(),
	aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices
	); //cargo la escena en el archivo con el importer
	
	if (scene) //Si se obtuvo algo
		InitFromScene(scene, texpath.c_str(), meshEntryVec, textures, boundingBox, rnd); //inicializo los meshes dentro de la escena
	else //sino hay un error
		cout << "Error Loading Mesh, Mesh wont be displayed." << endl; //error
}

void Model::LoadMesh(const string& modelPath, string& texpath, vector<MeshEntry>& meshEntryVec, vector<Header>& textures, vector<vector<vec3>> &boundingBox, Renderer* rnd)
{
	Assimp::Importer importer; //Creo el importer de Assimp
	const aiScene* scene = importer.ReadFile(
		modelPath.c_str(),
		aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices
	); //cargo la escena en el archivo con el importer

	if (scene) //Si se obtuvo algo
		InitFromScene(scene, texpath.c_str(), meshEntryVec, textures, boundingBox, rnd); //inicializo los meshes dentro de la escena
	else //sino hay un error
		cout << "Error Loading Mesh, Mesh wont be displayed." << endl; //error
}

void Model::InitFromScene(const aiScene* scene, const char* texpath, vector<MeshEntry>& vecMeshEntry, vector<Header>& textures, vector<vec3> &boundingBox, Renderer* rnd)
{
	vecMeshEntry.resize(scene->mNumMeshes); //Seteo el tamanio de los contenedores para la cantidad de componentes dentro de la escena.
	textures.resize(scene->mNumMaterials);
	vector<vec3> modelboundingbox;

	for (int e = 0; e < vecMeshEntry.size(); e++) //por cada uno de los meshes en la escena
	{
		InitMesh(e, scene->mMeshes[e], vecMeshEntry, modelboundingbox, rnd); //mando el mesh a inicializar

		for (size_t i = 0; i < modelboundingbox.size(); i++) //Vamos a sacar la bounding box de las bounding boxes, para sacar el area donde est� todo el conjunto de modelos.
		{
			if (boundingBox.size() < 8) // If its the first run of the for in the model, we need to populate all 8 vertexes of the box with something, lets use the first vertex.
			{
				for (size_t a = 0; a < 8; a++)
				{
					boundingBox.push_back(vec3(modelboundingbox[i].x, modelboundingbox[i].y, modelboundingbox[i].z));
				}
			}
			else
			{
				if (boundingBox[positionIndexEnum::TOPLEFTFRONT].x >= modelboundingbox[i].x && boundingBox[positionIndexEnum::TOPLEFTFRONT].y <= modelboundingbox[i].y && boundingBox
					[positionIndexEnum::TOPLEFTFRONT].z >= modelboundingbox[i].z)
					boundingBox[positionIndexEnum::TOPLEFTFRONT] = vec3(modelboundingbox[i].x, modelboundingbox[i].y, modelboundingbox[i].z);

				if (boundingBox[positionIndexEnum::TOPRIGHTFRONT].x <= modelboundingbox[i].x && boundingBox[positionIndexEnum::TOPRIGHTFRONT].y <= modelboundingbox[i].y && boundingBox[positionIndexEnum::TOPRIGHTFRONT].z >= modelboundingbox[i].z)
					boundingBox[positionIndexEnum::TOPRIGHTFRONT] = vec3(modelboundingbox[i].x, modelboundingbox[i].y, modelboundingbox[i].z);

				if (boundingBox[positionIndexEnum::BOTTOMLEFTFRONT].x >= modelboundingbox[i].x && boundingBox[positionIndexEnum::BOTTOMLEFTFRONT].y >= modelboundingbox[i].y && boundingBox[positionIndexEnum::BOTTOMLEFTFRONT].z >= modelboundingbox[i].z)
					boundingBox[positionIndexEnum::BOTTOMLEFTFRONT] = vec3(modelboundingbox[i].x, modelboundingbox[i].y, modelboundingbox[i].z);

				if (boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT].x <= modelboundingbox[i].x && boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT].y >= modelboundingbox[i].y && boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT].z >= modelboundingbox[i].z)
					boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT] = vec3(modelboundingbox[i].x, modelboundingbox[i].y, modelboundingbox[i].z);

				if (boundingBox[positionIndexEnum::TOPLEFTBACK].x >= modelboundingbox[i].x && boundingBox[positionIndexEnum::TOPLEFTBACK].y <= modelboundingbox[i].y && boundingBox
					[positionIndexEnum::TOPLEFTBACK].z <= modelboundingbox[i].z)
					boundingBox[positionIndexEnum::TOPLEFTBACK] = vec3(modelboundingbox[i].x, modelboundingbox[i].y, modelboundingbox[i].z);

				if (boundingBox[positionIndexEnum::TOPRIGHTBACK].x <= modelboundingbox[i].x && boundingBox[positionIndexEnum::TOPRIGHTBACK].y <= modelboundingbox[i].y && boundingBox
					[positionIndexEnum::TOPRIGHTBACK].z <= modelboundingbox[i].z)
					boundingBox[positionIndexEnum::TOPRIGHTBACK] = vec3(modelboundingbox[i].x, modelboundingbox[i].y, modelboundingbox[i].z);

				if (boundingBox[positionIndexEnum::BOTTOMLEFTBACK].x >= modelboundingbox[i].x && boundingBox[positionIndexEnum::BOTTOMLEFTBACK].y >= modelboundingbox[i].y && boundingBox[positionIndexEnum::BOTTOMLEFTBACK].z <= modelboundingbox[i].z)
					boundingBox[positionIndexEnum::BOTTOMLEFTBACK] = vec3(modelboundingbox[i].x, modelboundingbox[i].y, modelboundingbox[i].z);

				if (boundingBox[positionIndexEnum::BOTTOMRIGHTBACK].x <= modelboundingbox[i].x && boundingBox[positionIndexEnum::BOTTOMRIGHTBACK].y >= modelboundingbox[i].y && boundingBox[positionIndexEnum::BOTTOMRIGHTBACK].z <= modelboundingbox[i].z)
					boundingBox[positionIndexEnum::BOTTOMRIGHTBACK] = vec3(modelboundingbox[i].x, modelboundingbox[i].y, modelboundingbox[i].z);
			}
		}
	}

	if(texpath != "")
	for (int i = 0; i < scene->mNumMaterials; i++) //por cada uno de los materiales en la escena
		textures[i] = TextureImporter::loadBMP_custom(texpath); //cargo una textura en el
}

void Model::InitFromScene(const aiScene* scene, const char* texpath, vector<MeshEntry>& vecMeshEntry, vector<Header>& textures, vector<vector<vec3>> &boundingBox, Renderer* rnd)
{
	vecMeshEntry.resize(scene->mNumMeshes); //Seteo el tamanio de los contenedores para la cantidad de componentes dentro de la escena.
	textures.resize(scene->mNumMaterials);

	for (int e = 0; e < vecMeshEntry.size(); e++) //por cada uno de los meshes en la escena
	{
		InitMesh(e, scene->mMeshes[e], vecMeshEntry, boundingBox, rnd); //mando el mesh a inicializar
	}

	if (texpath != "")
		for (int i = 0; i < scene->mNumMaterials; i++) //por cada uno de los materiales en la escena
			textures[i] = TextureImporter::loadBMP_custom(texpath); //cargo una textura en el
}

void Model::InitMesh(unsigned int index, const aiMesh* mesh, std::vector<MeshEntry>& meshEntriesVec, vector<vec3> &boundingBox, Renderer* rnd) //inicializacion de los meshes individuales.
{
	std::vector<Vertex> Vertices; //para guardar los vertices del modelo
	std::vector<unsigned int> Indices; //para guardar los indices del modelo

	aiVector3D Zero3D(0.0f, 0.0f, 0.0f); //No se, estaba en el tuto. calculo que para inicializar.

	for (int i = 0; i < mesh->mNumVertices; i++) //por cada uno de los vertices...
	{
		aiVector3D* vectexCoord = mesh->HasTextureCoords(0) ? &(mesh->mTextureCoords[0][i]) : &Zero3D; //guardo las coordenadas de la textura. (En el tuto usan ese "?" que ni idea de que es, asi que no puedo simplificar esta funcion)

		Vertex v(vec3((mesh->mVertices[i]).x, (mesh->mVertices[i]).y, (mesh->mVertices[i]).z), //Guardo posiciones del vertice
			vec2((float)vectexCoord->x, (float)vectexCoord->y),
			vec3((float)(mesh->mNormals[i]).x, (float)(mesh->mNormals[i]).y, (float)(mesh->mNormals[i]).z)); //Guardo las normales
		// Creacion de un vertice que contiene todos los datos extraidos

		Vertices.push_back(v); //Se guarda en el Vector el vertice completo.
	}

	for (size_t i = 0; i < Vertices.size(); i++)
	{
		if (boundingBox.size() < 8) // If its the first run of the for in the model, we need to populate all 8 vertexes of the box with something, lets use the first vertex.
		{
			for (size_t e = 0; e < 8; e++)
			{
				boundingBox.push_back(vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z));
			}
		}
		else
		{
			if (boundingBox[positionIndexEnum::TOPLEFTFRONT].x >= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::TOPLEFTFRONT].y <= Vertices[i].vPosition.y && boundingBox
				[positionIndexEnum::TOPLEFTFRONT].z >= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::TOPLEFTFRONT] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::TOPRIGHTFRONT].x <= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::TOPRIGHTFRONT].y <= Vertices[i].vPosition.y && boundingBox[positionIndexEnum::TOPRIGHTFRONT].z >= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::TOPRIGHTFRONT] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::BOTTOMLEFTFRONT].x >= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::BOTTOMLEFTFRONT].y >= Vertices[i].vPosition.y && boundingBox[positionIndexEnum::BOTTOMLEFTFRONT].z >= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::BOTTOMLEFTFRONT] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT].x <= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT].y >= Vertices[i].vPosition.y && boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT].z >= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::TOPLEFTBACK].x >= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::TOPLEFTBACK].y <= Vertices[i].vPosition.y && boundingBox
				[positionIndexEnum::TOPLEFTBACK].z <= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::TOPLEFTBACK] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::TOPRIGHTBACK].x <= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::TOPRIGHTBACK].y <= Vertices[i].vPosition.y && boundingBox
				[positionIndexEnum::TOPRIGHTBACK].z <= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::TOPRIGHTBACK] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::BOTTOMLEFTBACK].x >= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::BOTTOMLEFTBACK].y >= Vertices[i].vPosition.y && boundingBox[positionIndexEnum::BOTTOMLEFTBACK].z <= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::BOTTOMLEFTBACK] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::BOTTOMRIGHTBACK].x <= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::BOTTOMRIGHTBACK].y >= Vertices[i].vPosition.y && boundingBox[positionIndexEnum::BOTTOMRIGHTBACK].z <= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::BOTTOMRIGHTBACK] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);
		}
	}

	for (int i = 0; i < mesh->mNumFaces; i++) //Esto es para las F que se rompian en el otro importer que no sirve para nada.
	{ //Datos de las caras
		aiFace& Face = mesh->mFaces[i];
		assert(Face.mNumIndices == 3);
		Indices.push_back(Face.mIndices[0]);
		Indices.push_back(Face.mIndices[1]);
		Indices.push_back(Face.mIndices[2]);
	}

	meshEntriesVec[index].Init(Vertices, Indices, rnd);
}

void Model::InitMesh(unsigned int index, const aiMesh* mesh, std::vector<MeshEntry>& meshEntriesVec, vector<vector<vec3>> &boundingBoxes, Renderer* rnd) //inicializacion de los meshes individuales.
{
	std::vector<Vertex> Vertices; //para guardar los vertices del modelo
	std::vector<unsigned int> Indices; //para guardar los indices del modelo
	vector<vec3> boundingBox;

	aiVector3D Zero3D(0.0f, 0.0f, 0.0f); //No se, estaba en el tuto. calculo que para inicializar.

	for (int i = 0; i < mesh->mNumVertices; i++) //por cada uno de los vertices...
	{
		aiVector3D* vectexCoord = mesh->HasTextureCoords(0) ? &(mesh->mTextureCoords[0][i]) : &Zero3D; //guardo las coordenadas de la textura. (En el tuto usan ese "?" que ni idea de que es, asi que no puedo simplificar esta funcion)

		Vertex v(vec3((mesh->mVertices[i]).x, (mesh->mVertices[i]).y, (mesh->mVertices[i]).z), //Guardo posiciones del vertice
			vec2((float)vectexCoord->x, (float)vectexCoord->y),
			vec3((float)(mesh->mNormals[i]).x, (float)(mesh->mNormals[i]).y, (float)(mesh->mNormals[i]).z)); //Guardo las normales
																											 // Creacion de un vertice que contiene todos los datos extraidos

		Vertices.push_back(v); //Se guarda en el Vector el vertice completo.
	}

	for (size_t i = 0; i < Vertices.size(); i++)
	{
		if (boundingBox.size() < 8) // If its the first run of the for in the model, we need to populate all 8 vertexes of the box with something, lets use the first vertex.
		{
			for (size_t e = 0; e < 8; e++)
			{
				boundingBox.push_back(vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z));
			}
		}
		else
		{
			if (boundingBox[positionIndexEnum::TOPLEFTFRONT].x >= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::TOPLEFTFRONT].y <= Vertices[i].vPosition.y && boundingBox
				[positionIndexEnum::TOPLEFTFRONT].z >= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::TOPLEFTFRONT] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::TOPRIGHTFRONT].x <= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::TOPRIGHTFRONT].y <= Vertices[i].vPosition.y && boundingBox[positionIndexEnum::TOPRIGHTFRONT].z >= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::TOPRIGHTFRONT] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::BOTTOMLEFTFRONT].x >= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::BOTTOMLEFTFRONT].y >= Vertices[i].vPosition.y && boundingBox[positionIndexEnum::BOTTOMLEFTFRONT].z >= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::BOTTOMLEFTFRONT] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT].x <= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT].y >= Vertices[i].vPosition.y && boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT].z >= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::BOTTOMRIGHTFRONT] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::TOPLEFTBACK].x >= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::TOPLEFTBACK].y <= Vertices[i].vPosition.y && boundingBox
				[positionIndexEnum::TOPLEFTBACK].z <= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::TOPLEFTBACK] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::TOPRIGHTBACK].x <= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::TOPRIGHTBACK].y <= Vertices[i].vPosition.y && boundingBox
				[positionIndexEnum::TOPRIGHTBACK].z <= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::TOPRIGHTBACK] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::BOTTOMLEFTBACK].x >= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::BOTTOMLEFTBACK].y >= Vertices[i].vPosition.y && boundingBox[positionIndexEnum::BOTTOMLEFTBACK].z <= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::BOTTOMLEFTBACK] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);

			if (boundingBox[positionIndexEnum::BOTTOMRIGHTBACK].x <= Vertices[i].vPosition.x && boundingBox[positionIndexEnum::BOTTOMRIGHTBACK].y >= Vertices[i].vPosition.y && boundingBox[positionIndexEnum::BOTTOMRIGHTBACK].z <= Vertices[i].vPosition.z)
				boundingBox[positionIndexEnum::BOTTOMRIGHTBACK] = vec3(Vertices[i].vPosition.x, Vertices[i].vPosition.y, Vertices[i].vPosition.z);
		}
	}

	for (int i = 0; i < mesh->mNumFaces; i++) //Esto es para las F que se rompian en el otro importer que no sirve para nada.
	{ //Datos de las caras
		aiFace& Face = mesh->mFaces[i];
		assert(Face.mNumIndices == 3);
		Indices.push_back(Face.mIndices[0]);
		Indices.push_back(Face.mIndices[1]);
		Indices.push_back(Face.mIndices[2]);
	}

	meshEntriesVec[index].Init(Vertices, Indices, rnd);
	boundingBoxes.push_back(boundingBox);
}