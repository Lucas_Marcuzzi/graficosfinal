#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "TextureImporter.h"
#include "Exports.h"
#include "ModelImporter.h"
#include "assimp\Importer.hpp"
#include "assimp\postprocess.h"
#include "assimp\scene.h"
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "glm\glm.hpp"
#include "glm\glm.hpp"
#include "Coso.h"


class ENGINEDLL_API Model
{
	Model();
	~Model();
	static Model *instance; //esta instancia de Model
	aiScene* scene; //la escena que contiene el archivo
	Assimp::Importer importer; //el importer de Assimp
	void InitFromScene(const aiScene* scene, const char* texpath, vector<MeshEntry>& vecMeshEntry, vector<Header>& textures, vector<vec3> &boundingBox, Renderer* rnd);
	void InitFromScene(const aiScene* scene, const char* texpath, vector<MeshEntry>& vecMeshEntry, vector<Header>& textures, vector<vector<vec3>> &boundingBox, Renderer* rnd);
	void InitMesh(unsigned int index, const aiMesh* mesh, vector<MeshEntry>& meshEntryVec, vector<vec3> &boundingBox, Renderer* rnd);
	void InitMesh(unsigned int index, const aiMesh* mesh, vector<MeshEntry>& meshEntryVec, vector<vector<vec3>> &boundingBox, Renderer* rnd);

public:
	void LoadMesh(const string& modelPath, string& texPath, vector<MeshEntry>& meshEntriesVec, vector<Header>& textures, vector<vec3> &boundingBox, Renderer* rnd);
	void LoadMesh(const string& modelPath, string& texPath, vector<MeshEntry>& meshEntriesVec, vector<Header>& textures, vector<vector<vec3>> &boundingBox, Renderer* rnd);
	static Model* GetInstance();
};
