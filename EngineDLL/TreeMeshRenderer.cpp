#include "TreeMeshRenderer.h"

TreeMeshRenderer::TreeMeshRenderer(Transform * _transform, Renderer * _rnd)
{
	key = "TreeMeshRenderer";
	rnd = _rnd;
	transform = _transform;
}

TreeMeshRenderer::~TreeMeshRenderer()
{
}

void TreeMeshRenderer::Update()
{
	rnd->loadIdentityMatrix();
	rnd->MultiplyModelMatrix(transform->GetModelMatrix());
	if (material != NULL) {
		material->Bind();
		material->SetMatrixProperty("MVP", rnd->GetMVP());
	}
	rnd->EnableAttributes(0);
	rnd->EnableAttributes(1);
	rnd->BindBuffer(vertexBuffer, 0);
	rnd->BindTextureBuffer(uvBuffer, 1);
	rnd->BindMeshBuffer(indexBuffer);
	rnd->DrawIndex(veccantIndex.size());
	rnd->DisableAttributes(0);
	rnd->DisableAttributes(1);
}

void TreeMeshRenderer::SetVertexes(std::vector<float> _vertexes)
{
	vecVertices = _vertexes;
	vertexBuffer = rnd->GenBuffer(&vecVertices[0], vecVertices.size() * sizeof(float));
}

void TreeMeshRenderer::setUVs(std::vector<float> _uvs)
{
	vecUvs = _uvs;
	uvBuffer = rnd->GenBuffer(&vecUvs[0], vecUvs.size() * sizeof(float));
}

void TreeMeshRenderer::setCantIndex(std::vector<unsigned int> _veccantIndex)
{
	veccantIndex = _veccantIndex;
	indexBuffer = rnd->GenIndexBuffer(veccantIndex);
}

void TreeMeshRenderer::SetTexture(string _texpath)
{
		texPath = _texpath;
		textures = TextureImporter::loadBMP_custom(_texpath.c_str()); //cargo una textura en el
		textureBuffer = rnd->GenTexture(textures.width, textures.height, textures.data);
}

void TreeMeshRenderer::LoadMaterial()
{
	material = new Material();
	materialID = material->LoadShader("Shaders\\SimpleVertexShader.vertexshader"		// Vertex Shader
		, "Shaders\\SimpleFragmentShader.fragmentshader"	// Fragment Shader
	);
}
void TreeMeshRenderer::LoadMaterial(Material* _mat)
{
	material = _mat;
}
