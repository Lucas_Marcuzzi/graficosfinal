#include "MeshRenderer.h"
#pragma warning(disable : 4996)


MeshRenderer::MeshRenderer(Transform* _transform, Renderer* _renderer, Material* _material, string filepath, string texturePath)
{
	key = "MeshRenderer";
	transform = _transform;
	renderer = _renderer;
	path = new char[texturePath.size() + 1];
	texturePath.copy(path, texturePath.size() + 1);
	path[texturePath.size()] = '\0';

	Model::GetInstance()->LoadMesh(filepath, texturePath, vecMeshEntry, vecHeaders, boundingBox, renderer); //llama a loadMesh de la clase Model
	material = _material;

	for (int i = 0; i < vecHeaders.size(); i++)
		bufferTextureID.push_back(renderer->GenTexture(vecHeaders[i].width, vecHeaders[i].height, vecHeaders[i].data));

	renderer->dmm.IncreaseMeshCount();
}

MeshRenderer::MeshRenderer(Transform * _transform, Renderer * _renderer, Material * _material, CameraComponent * camera, string filepath, string texturePath)
{
	key = "MeshRenderer";
	transform = _transform;
	renderer = _renderer;
	path = new char[texturePath.size() + 1];
	texturePath.copy(path, texturePath.size() + 1);
	path[texturePath.size()] = '\0';

	Model::GetInstance()->LoadMesh(filepath, texturePath, vecMeshEntry, vecHeaders, boundingBox, renderer); //llama a loadMesh de la clase Model
	material = _material;

	for (int i = 0; i < vecHeaders.size(); i++)
		bufferTextureID.push_back(renderer->GenTexture(vecHeaders[i].width, vecHeaders[i].height, vecHeaders[i].data));

	frustumCamera = camera;

	renderer->dmm.IncreaseMeshCount();
}


MeshRenderer::~MeshRenderer()
{
}

vector<vec3>* MeshRenderer::GetBoundingBox()
{
	return &boundingBox;
}

void MeshRenderer::Update()
{
	vector<vec3> _bbox = boundingBox;
	for (size_t i = 0; i < _bbox.size(); i++)
	{
		_bbox.at(i) *= transform->GetScale();
		_bbox.at(i) *= transform->GetRotation();
		_bbox.at(i) += transform->GetPosition();
	}

	if (frustumCamera != nullptr)
	{
		if (frustumCamera->FrustumCheck(&_bbox) && frustumCamera->BSPCheck(&_bbox))
		{
			renderer->dmm.IncreaseDrawnMeshCount();
			Draw();
		}
	}
	else
	{
		renderer->dmm.IncreaseDrawnMeshCount();
		Draw();
	}
	
}

void MeshRenderer::UpdateBoundingBox()
{
	for (size_t i = 0; i < boundingBox.size(); i++)
	{
		boundingBox.at(i) *= transform->GetPosition();
	}
}

void MeshRenderer::Draw()
{
	//renderer->loadIdentityMatrix();
	renderer->MultiplyModelMatrix(transform->GetModelMatrix());
	if (material != NULL) {
		material->Bind();
		material->SetMatrixProperty("MVP", renderer->GetMVP());
	}
	renderer->EnableAttributes(0);
	renderer->EnableAttributes(1);
	for (int i = 0; i < vecMeshEntry.size(); i++) {
		renderer->BindBuffer(vecMeshEntry[i].vertexBuffer, 0);
		renderer->BindTextureBuffer(vecMeshEntry[i].uvBuffer, 1);
		renderer->BindMeshBuffer(vecMeshEntry[i].indexBuffer);
		renderer->DrawIndex(vecMeshEntry[i].cantIndex);
	}
	renderer->DisableAttributes(0);
	renderer->DisableAttributes(1);
}
