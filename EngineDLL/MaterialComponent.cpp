#include "MaterialComponent.h"



MaterialComponent::MaterialComponent()
{
	key = "MaterialComponent";
	material = new Material();
	material->LoadShader("Shaders\\SimpleVertexShader.vertexshader", "Shaders\\SimpleFragmentShader.fragmentshader");
	newMaterialCreated = true;
}

MaterialComponent::MaterialComponent(Material * _material)
{
	key = "MaterialComponent";
	material = _material;
	newMaterialCreated = false;
}


MaterialComponent::~MaterialComponent()
{
	if (newMaterialCreated)
		delete material;
}

void MaterialComponent::Update()
{
}

Material * MaterialComponent::GetMaterial()
{
	return material;
}

