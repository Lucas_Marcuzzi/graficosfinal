#pragma once

enum FrustumPlanesEnums
{
	NEAR,
	FAR,
	LEFT,
	TOP,
	RIGHT,
	BOTTOM,
	CANT = 6
};