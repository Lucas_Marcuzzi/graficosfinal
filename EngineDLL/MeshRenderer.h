#pragma once
#include "Exports.h"
#include "Componente.h"
#include "MaterialComponent.h"
#include "Shape.h"
#include "Definitions.h"
#include "ModelImporter.h"
#include "Model.h"
#include "TextureImporter.h"
#include "Transform.h"
#include "CameraComponent.h"
#include "DebugMessageManager.h"

class ENGINEDLL_API MeshRenderer :
	public Componente
{
protected:
	char* path;
	vector<MeshEntry> vecMeshEntry;
	vector<Header> vecHeaders;
	vector<vec3> boundingBox;
	vector<unsigned int> bufferTextureID;
	Renderer* renderer;
	Transform* transform;
	Material* material;
	CameraComponent* frustumCamera = nullptr;

public:
	MeshRenderer::MeshRenderer(Transform* _transform, Renderer* _renderer, Material* _material, string filepath = "cube.obj", string texturePath = "default.bmp");
	MeshRenderer::MeshRenderer(Transform* _transform, Renderer* _renderer, Material* _material, CameraComponent* camera, string filepath = "cube.obj", string texturePath = "default.bmp");
	~MeshRenderer();
	vector<vec3>* GetBoundingBox();
	void Update() override;
	void UpdateBoundingBox();
	void Draw();
};
