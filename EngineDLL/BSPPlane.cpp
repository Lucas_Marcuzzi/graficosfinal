#include "BSPPlane.h"
#pragma warning(disable : 4996)


BSPPlane::BSPPlane(Transform* _transform, Renderer* _renderer, Material* _material, string filepath, string texturePath)
{
	key = "BSPPlane";
	transform = _transform;
	renderer = _renderer;
	path = new char[texturePath.size() + 1];
	texturePath.copy(path, texturePath.size() + 1);
	path[texturePath.size()] = '\0';

	Model::GetInstance()->LoadMesh(filepath, texturePath, vecMeshEntry, vecHeaders, boundingBox, renderer); //llama a loadMesh de la clase Model
	material = _material;

	for (int i = 0; i < vecHeaders.size(); i++)
		bufferTextureID.push_back(renderer->GenTexture(vecHeaders[i].width, vecHeaders[i].height, vecHeaders[i].data));

	planeid = renderer->GetBSPList()->size();
	renderer->AddBSP(&boundingBox);
	renderer->InitializeNeighbors();
	ogBoundingBox = boundingBox;
}


BSPPlane::~BSPPlane()
{
}

vector<vec3>* BSPPlane::GetBoundingBox()
{
	return &boundingBox;
}

void BSPPlane::Update()
{

	boundingBox = ogBoundingBox;
	for (size_t i = 0; i < boundingBox.size(); i++)
	{
		boundingBox.at(i) *= transform->GetScale();
		boundingBox.at(i) += transform->GetPosition();
	}
		Draw();

}

void BSPPlane::Draw()
{

	if (renderer->debugShowBSP)
	{
		//renderer->loadIdentityMatrix();
		renderer->MultiplyModelMatrix(transform->GetModelMatrix());
		if (material != NULL) {
			material->Bind();
			material->SetMatrixProperty("MVP", renderer->GetMVP());
		}
		renderer->EnableAttributes(0);
		renderer->EnableAttributes(1);
		for (int i = 0; i < vecMeshEntry.size(); i++) {
			renderer->BindBuffer(vecMeshEntry[i].vertexBuffer, 0);
			renderer->BindTextureBuffer(vecMeshEntry[i].uvBuffer, 1);
			renderer->BindMeshBuffer(vecMeshEntry[i].indexBuffer);
			renderer->DrawIndex(vecMeshEntry[i].cantIndex);
		}
		renderer->DisableAttributes(0);
		renderer->DisableAttributes(1);
	}
}

void BSPPlane::AddNeighbor(vector<vec3>* _new)
{
	renderer->AddNeighbors(_new, planeid);
}
