#pragma once
#include "Componente.h"
#include "Renderer.h"
#include "glm\exponential.hpp"
#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "glm\gtx\transform.hpp"
#include "Transform.h"
#include "Exports.h"
#include "FrustumPlanesEnums.h"
#include "BoundingBoxEnums.h"
#include "DebugMessageManager.h"
	
#define GLM_ENABLE_EXPERIMENTAL

using namespace glm;
class ENGINEDLL_API CameraComponent :
	public Componente
{
private:
	Renderer * renderer;
	vec3 eyePosition;
	vec3 cameraPosition;
	vec3 upVector;
	vec4 forward;
	vec4 right;
	vec4 up;

	//Frustum Variables
	vec4 frustumPlanes[CANT];

	float near;
	float far;
	float aspectRatio;
	float fov;
	float tang;
	float nearWidht;
	float nearHeight;
	float aspectRatioWidth;
	float aspectRatioHeight;

	bool activeBSP;

protected:
	Transform* transform;
public:
	vec4 GeneratePlane(vec3 _normal, vec3 _point);
	void SetFrustum();
	void SetAspectRatio(float _height, float _width);
	bool FrustumCheck(vector<vec3>* boundingBox);
	void SetBSPActive(bool active);
	void Strafe(float x, float y);
	void SetOrtho();
	void SetPerspective();
	void Walk(int ammount);
	void StrafeAdvance(int ammount);
	void Pitch(float ammount);
	void Yaw(float ammount);
	void Roll(float ammount);

	//BSP
	vector<vector<vec3>*> GetCurrentBSP();
	bool BSPCheck(vector<vec3>* boundingBox);

	CameraComponent(Transform* _transform, Renderer* _renderer);
	void Update() override;
	~CameraComponent();
};

