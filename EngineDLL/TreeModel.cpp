#include "TreeModel.h"


TreeModel::TreeModel(const string & modelPath, string & texpath, Renderer * rnd, Material* _mtl)
{
	Assimp::Importer importer; //Creo el importer de Assimp
	const aiScene* scene = importer.ReadFile(
		modelPath.c_str(),
		aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices
	); //cargo la escena en el archivo con el importer

	if (scene) //Si se obtuvo algo
	{
	//InitFromScene(scene, texpath.c_str(), meshEntryVec, textures, rnd); //inicializo los meshes dentro de la escena
		Coso* primerCoso = new Coso(rnd,"primerCoso");
		GenerateTree(scene, primerCoso, scene->mRootNode, texpath, rnd,_mtl);
		generatedCoso = primerCoso;
	}
	else //sino hay un error
		cout << "Error Loading Mesh, Mesh wont be displayed." << endl; //error
	
}

void TreeModel::InitMesh(const aiMesh* mesh, TreeMeshRenderer* _tmr, Renderer* rend)
{
	std::vector<float> vertices;
	std::vector<float> uvs;
	std::vector<unsigned int> facesIndexes;

	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		aiVector3D pos = mesh->mVertices[i];
		vertices.push_back(pos.x);
		vertices.push_back(pos.y);
		vertices.push_back(pos.z);

		aiVector3D uv = mesh->mTextureCoords[0][i];
		uvs.push_back(uv.x);
		uvs.push_back(uv.y);
	}

	for (unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		facesIndexes.push_back(mesh->mFaces[i].mIndices[0]);
		facesIndexes.push_back(mesh->mFaces[i].mIndices[1]);
		facesIndexes.push_back(mesh->mFaces[i].mIndices[2]);
	}

	_tmr->SetVertexes(vertices);
	_tmr->setUVs(uvs);
	_tmr->setCantIndex(facesIndexes);

}

Coso * TreeModel::GetGeneratedCoso()
{
	if(generatedCoso)
		return generatedCoso;
	else
		_RPT0(_CRT_ERROR, "Trying to get a coso that was not generated");
}

void TreeModel::GenerateTree(const aiScene * scene, Coso * _primerCoso, aiNode * root, const string & texturePath, Renderer * rnd, Material* _mtl)
{
	for (int i = 0; i < root->mNumChildren; i++)
	{
		Coso* childCoso = new Coso(rnd, "childCoso");
		_primerCoso->AddChild(childCoso);
		if (root->mChildren[i]->mNumMeshes > 0)
		{
			childCoso->AddComponent(new TreeMeshRenderer(childCoso->GetTransform(), rnd));
			TreeMeshRenderer* tmr = (TreeMeshRenderer*)childCoso->GetComponent("TreeMeshRenderer");
			unsigned int index = root->mChildren[i]->mMeshes[0];
			InitMesh(scene->mMeshes[index], tmr, rnd);
			//tmr->SetTexture(texturePath);
			tmr->LoadMaterial(_mtl);
			if (root->mChildren[i]->mNumChildren > 0)
			{
				GenerateTree(scene, childCoso, root->mChildren[i], texturePath, rnd, _mtl);
			}
			
		}
	}
}

TreeModel::~TreeModel()
{
}
