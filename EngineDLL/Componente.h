#pragma once
#include "Exports.h"
#include <string>
#include <vector>

class ENGINEDLL_API Componente
{
protected:
	std::string key;
public:
	Componente();
	~Componente();
	virtual void Update();
	std::string GetKey();
};

