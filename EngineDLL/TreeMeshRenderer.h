#pragma once
#include "Componente.h"
#include "Exports.h"
#include "Material.h"
#include "TextureImporter.h"
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <assimp/Importer.hpp> 
#include <assimp/scene.h>           
#include <assimp/postprocess.h>
#include "Transform.h"

class ENGINEDLL_API TreeMeshRenderer : public Componente
{
protected:
	Renderer* rnd;
	Transform* transform;
	std::string filePath;
	std::string texPath;
	Header textures;
	Material* material;
	std::vector<float> vecVertices;
	std::vector<float> vecUvs;
	std::vector<unsigned int> veccantIndex;
	unsigned int materialID;
	unsigned int vertexBuffer;
	unsigned int uvBuffer;
	unsigned int indexBuffer;
	unsigned int textureBuffer;
public:
	TreeMeshRenderer(Transform * _transform, Renderer* _rnd);
	~TreeMeshRenderer();
	void Update() override;
	void SetVertexes(std::vector<float> _vertexes);
	void setUVs(std::vector<float> _uvs);
	void setCantIndex(std::vector<unsigned int> _vcantIndex);
	void SetTexture(string _texpath);
	void LoadMaterial();
	void LoadMaterial(Material* _mat);
};

