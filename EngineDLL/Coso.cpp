#include "Coso.h"



Coso::Coso(Renderer* rnd, string _id)
{
	componentes.push_back(new Transform());
	id = _id;
	renderer = rnd;
	aux = glm::mat4(1.0);
	identity = glm::mat4(1.0);
	transform = (Transform*)(GetComponent("Transform"));
}


Coso::~Coso()
{
	Componente* auxcomponente;
	Coso* auxhijo;
	while (!componentes.empty()) {
		auxcomponente = componentes.back();
		delete auxcomponente;
		componentes.pop_back();
	}
	while (!hijos.empty()) {
		auxhijo = hijos.back();
		delete auxhijo;
		hijos.pop_back();
	}
}

void Coso::AddComponent(Componente* _component)
{
	bool ComponentAlreadyExistsWhithinCoso = false;

	for (int i = 0; i < componentes.size(); i++)
		if (componentes[i]->GetKey() == _component->GetKey())
		{
			_RPT0(_CRT_ERROR, "Trying to add a component that already exists whithin a Coso");
			return;
		}
	componentes.push_back(_component);
}

void Coso::AddChild(Coso* _hijo)
{
	_hijo->SetParent(this);
	hijos.push_back(_hijo);
}

void Coso::Update()
{
	aux = renderer->GetMVP();
	if (parent == nullptr)
		//renderer->MultiplyModelMatrix(identity);
		renderer->loadIdentityMatrix();
	/*else
	{
		Transform* parentTransform = (Transform*)(parent->GetComponent("Transform"));
		renderer->MultiplyModelMatrix(parentTransform->GetModelMatrix());
	}*/
	for (int i = 0; i < componentes.size(); i++) {
		componentes[i]->Update();
	}
	for (int i = 0; i < hijos.size(); i++) {
		hijos[i]->Update();
	}
	renderer->SetModelMatrix(aux);
}

Componente* Coso::GetComponent(std::string _id)
{
	for (int i = 0; i < componentes.size(); i++) {
		if (componentes[i]->GetKey() == _id)
			return componentes[i];
	}
	return NULL;
}

void Coso::SetParent(Coso * _parent)
{
	parent = _parent;
}

vector<Coso*> Coso::GetChildren()
{
	return hijos;
}

Coso * Coso::GetParent()
{
	return parent;
}

Renderer * Coso::GetRenderer()
{
	return renderer;
}

Transform * Coso::GetTransform()
{
	return transform;
}