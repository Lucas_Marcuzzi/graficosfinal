#include "BSPData.h"



BSPData::BSPData()
{
}


BSPData::~BSPData()
{
}

void BSPData::SetBoundingBox(vector<vec3>* _BoundingBox)
{
	boundingBox = _BoundingBox;
}

vector<vec3>* BSPData::GetBoundingBox()
{
	return boundingBox;
}

void BSPData::AddNeighbor(vector<vec3> _neighbor)
{
	neighbors.push_back(_neighbor);
}

vector<vector<vec3>> BSPData::GetNeighbors()
{
	return neighbors;
}

void BSPData::SetName(string _name)
{
	name = _name;
}

string BSPData::GetName()
{
	return name;
}
