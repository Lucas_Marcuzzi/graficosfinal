#pragma once
#include "Shape.h"
#include "TextureImporter.h"

class ENGINEDLL_API Cube :
	public Shape
{
	float * colorVertex;
	bool shouldDisposeColor;
	unsigned int * indxvertex;
public:
	Cube(Renderer* renderer, Material* material, Layers tag);
	~Cube();
	void Draw();
	void DrawMesh(int type);
};

