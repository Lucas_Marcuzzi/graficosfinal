#pragma once

#include "glm\glm.hpp"
#include "Exports.h"
#include <string>
#include <vector>

using namespace glm;
using namespace std;

class ENGINEDLL_API BSPData
{
	
	vector<vec3>* boundingBox;
	string name;
	vector<vector<vec3>> neighbors;

public:
	BSPData();
	~BSPData();
	void SetBoundingBox(vector<vec3>* _BoundingBox);
	vector<vec3>* GetBoundingBox();
	void AddNeighbor(vector<vec3> _neighbor);
	vector<vector<vec3>> GetNeighbors();
	void SetName(string _name);
	string GetName();
};

