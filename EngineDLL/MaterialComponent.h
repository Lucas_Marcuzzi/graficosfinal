#pragma once
#include "Exports.h"
#include "Componente.h"
#include "Material.h"

class ENGINEDLL_API MaterialComponent :
	public Componente
{
private:
	Material* material;
	bool newMaterialCreated;
public:
	MaterialComponent();
	MaterialComponent(Material* _material);
	~MaterialComponent();
	void Update() override;
	Material* GetMaterial();
};

