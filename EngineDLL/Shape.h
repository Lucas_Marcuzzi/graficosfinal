#pragma once

#include <time.h> // Used for random purposes
#include "Entity.h"

class Shape : public Entity
{
protected:
	float* verticesData;	// Data of the Vertices
	int count;				// Total vertices
	int variables;			// Total data for each vertice
	int indexVCount;
	int colorvertexcount;
	unsigned int indexBufferId;
	bool shouldDisposeColor;

	void Update();
public:
	virtual void Draw() = 0;
	void ShouldDispose() override;

	unsigned int SetVertices(
		float* vertices,	// Data of the vertices
		int _count			// Total Vertices
	) override;
	unsigned int SetVertices(vector<vec3> vertices, int _count);
	void SetColorVertices(float* vertices, int count);
	void SetIndexVertices(unsigned int * vertices, int count);
	void DisposeColor();
	void Dispose();
	Shape(Renderer* renderer, Material* material, Layers tag);
	~Shape();
};

