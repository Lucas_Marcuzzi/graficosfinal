#pragma once
#include "Exports.h"
#include "Componente.h"
#include "MaterialComponent.h"
#include "Shape.h"
#include "Definitions.h"
#include "ModelImporter.h"
#include "Model.h"
#include "TextureImporter.h"
#include "Transform.h"
#include "CameraComponent.h"

class ENGINEDLL_API BSPPlaneBulk :
	public Componente
{
protected:
	char* path;
	vector<MeshEntry> vecMeshEntry;
	vector<Header> vecHeaders;
	vector<vector<vec3>> boundingBoxes;
	vector<vector<vec3>> ogBoundingBoxes;
	vector<unsigned int> bufferTextureID;
	Renderer* renderer;
	Transform* transform;
	Material* material;
	CameraComponent* frustumCamera = nullptr;
public:
	BSPPlaneBulk::BSPPlaneBulk(Transform* _transform, Renderer* _renderer, Material* _material, string filepath = "cube.obj", string texturePath = "default.bmp");
	~BSPPlaneBulk();
	vector<vector<vec3>>* GetBoundingBox();
	void Update() override;
	void Draw();
};
