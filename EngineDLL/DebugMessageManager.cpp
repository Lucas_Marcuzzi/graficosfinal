#include "DebugMessageManager.h"


DebugMessageManager::DebugMessageManager()
{
}


DebugMessageManager::~DebugMessageManager()
{
}

void DebugMessageManager::IncreaseMeshCount()
{
	meshCount++;
}

void DebugMessageManager::IncreaseDrawnMeshCount()
{
	drawnMeshCount++;
}

void DebugMessageManager::SetMode(int _mode)
{
	mode = _mode;
}

void DebugMessageManager::SetInBSP(bool _status)
{
	inBSP = _status;
}

void DebugMessageManager::SetUnlockedY(bool _unlocked)
{
	isYUnlocked = _unlocked;
}

void DebugMessageManager::Update()
{
	switch (mode)
	{
	case NONE:
		break;
	case MESHCOUNT:
		system("CLS");
		std::cout << "Meshes drawn: " << drawnMeshCount << "/" << meshCount << std::endl;
		break;
	case BSP:
		system("CLS");
		if (inBSP)
			std::cout << "Currently in BSP plane" << std::endl;
		if (isYUnlocked)
			std::cout << "BSP Y Axis is currently UNLOCKED." << std::endl;
		else
			std::cout << "BSP Y Axis is currently LOCKED." << std::endl;
		

	}
}

void DebugMessageManager::Reset()
{
	drawnMeshCount = 0;
}
