#include "BSPPlaneBulk.h"
#pragma warning(disable : 4996)


BSPPlaneBulk::BSPPlaneBulk(Transform* _transform, Renderer* _renderer, Material* _material, string filepath, string texturePath)
{
	key = "BSPPlaneBulk";
	transform = _transform;
	renderer = _renderer;
	path = new char[texturePath.size() + 1];
	texturePath.copy(path, texturePath.size() + 1);
	path[texturePath.size()] = '\0';

	Model::GetInstance()->LoadMesh(filepath, texturePath, vecMeshEntry, vecHeaders, boundingBoxes, renderer); //llama a loadMesh de la clase Model
	material = _material;

	for (int i = 0; i < vecHeaders.size(); i++)
		bufferTextureID.push_back(renderer->GenTexture(vecHeaders[i].width, vecHeaders[i].height, vecHeaders[i].data));

	for (size_t i = 0; i < boundingBoxes.size(); i++)
	{
		renderer->AddBSP(&boundingBoxes.at(i));
	}
	
	ogBoundingBoxes = boundingBoxes;
}


BSPPlaneBulk::~BSPPlaneBulk()
{
}

vector<vector<vec3>>* BSPPlaneBulk::GetBoundingBox()
{
	return &boundingBoxes;
}

void BSPPlaneBulk::Update()
{

	boundingBoxes = ogBoundingBoxes;
	for (size_t i = 0; i < boundingBoxes.size(); i++)
	{
		for (size_t e = 0; e < boundingBoxes.at(i).size(); e++)
		{
			boundingBoxes.at(i).at(e) *= transform->GetScale();
			boundingBoxes.at(i).at(e) += transform->GetPosition();
		}
	}
	Draw();

}

void BSPPlaneBulk::Draw()
{

	if (renderer->debugShowBSP)
	{
		//renderer->loadIdentityMatrix();
		renderer->MultiplyModelMatrix(transform->GetModelMatrix());
		if (material != NULL) {
			material->Bind();
			material->SetMatrixProperty("MVP", renderer->GetMVP());
		}
		renderer->EnableAttributes(0);
		renderer->EnableAttributes(1);
		for (int i = 0; i < vecMeshEntry.size(); i++) {
			renderer->BindBuffer(vecMeshEntry[i].vertexBuffer, 0);
			renderer->BindTextureBuffer(vecMeshEntry[i].uvBuffer, 1);
			renderer->BindMeshBuffer(vecMeshEntry[i].indexBuffer);
			renderer->DrawIndex(vecMeshEntry[i].cantIndex);
		}
		renderer->DisableAttributes(0);
		renderer->DisableAttributes(1);
	}
}
