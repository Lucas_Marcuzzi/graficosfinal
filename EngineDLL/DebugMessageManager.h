#pragma once
#include "Exports.h"
#include <stdlib.h>
#include <iostream>

enum DebugModes
{
	NONE,
	MESHCOUNT,
	BSP,
	GRAFICOS3,
	GRAFICOS4,
};

class ENGINEDLL_API DebugMessageManager
{

private:
	int mode;
	int drawnMeshCount;
	int meshCount;
	bool inBSP;
	bool isYUnlocked;

public:

	int finalExam = GRAFICOS3;
	bool neighborSystem = false;
	bool bulkMode = false;

	DebugMessageManager();
	~DebugMessageManager();
	void IncreaseMeshCount();
	void IncreaseDrawnMeshCount();
	void SetMode(int _mode);
	void SetInBSP(bool _status);
	void SetUnlockedY(bool _unlocked);
	void Update();
	void Reset();
};

